#!/usr/bin/env python
''' Unit Test for proles.py '''
import unittest
import proles

class DateTest(unittest.TestCase):
    ''' Test class for date string '''
    def test_date(self):
        ''' Testing given date for correct format '''
        print('testing date strings')
        self.assertEqual(proles.sanitize_date('1.2'), '01.02.2017')
        self.assertEqual(proles.sanitize_date('1,2,20'), '01.02.2020')
        self.assertEqual(proles.sanitize_date('01.02 2017'), '01.02.2017')
        self.assertEqual(proles.sanitize_date('01 2 2019'), '01.02.2019')

        with self.assertRaises(ValueError) as context:
            proles.sanitize_date('1.2.4000')
        #print(context.exception)
        self.assertTrue('out of range' in str(context.exception).lower())

        with self.assertRaises(ValueError) as context:
            proles.sanitize_date('1.2.200')
        #print(context.exception)
        self.assertTrue('wrong date format' in str(context.exception).lower())

        with self.assertRaises(ValueError) as context:
            proles.sanitize_date('1')
        #print(context.exception)
        self.assertTrue('wrong number' in str(context.exception).lower())


class TimeTest(unittest.TestCase):
    ''' Test class for time string '''
    def test_time(self):
        ''' Testing given start and end times for correct format '''
        print('\ntesting time strings')
        '''
        self.assertEqual('0700-1200', proles.sanitize_time('700-12'))
        self.assertEqual('0700-1200', proles.sanitize_time('0700-1200'))
        self.assertEqual('0700-1200', proles.sanitize_time('07 12'))
        self.assertEqual('0700-1200', proles.sanitize_time('7:00 12:00'))
        self.assertEqual('0700-1200', proles.sanitize_time('7-12'))
        self.assertEqual('0700-1200', proles.sanitize_time('7 12'))
        self.assertEqual('0700-1200', proles.sanitize_time('7 12 13-17'))
        '''

        self.assertEqual(['0700', '1200'], proles.sanitize_time('7 12'))
        self.assertEqual(['0700', '1200', '1300', '1700'], proles.sanitize_time('7 12 13-17'))

        with self.assertRaises(ValueError) as context:
            proles.sanitize_time('1300-2401')
        #print(context.exception)
        self.assertTrue('not valid' in str(context.exception).lower())

        with self.assertRaises(ValueError) as context:
            proles.sanitize_time('1200-700')
        #print(context.exception)
        self.assertTrue('later' in str(context.exception).lower())

        with self.assertRaises(ValueError) as context:
            proles.sanitize_time('700')
        #print(context.exception)
        self.assertTrue('odd' in str(context.exception).lower())


class SplitTest(unittest.TestCase):
    ''' Test class for splitting times-list '''
    def test_split(self):
        ''' Test splitting of returned times-list '''
        print('\nTest splitting of times-list')

        self.assertEqual(['0700-1200', '1300-1700'], proles.split_time(['0700', '1200', '1300', '1700']))
        print(proles.split_time(['0700', '1200', '1300', '1700']))



class CreateDataTest(unittest.TestCase):
    ''' Test class for generating valid datasets '''
    def test_create(self):
        day_entry = {'Date': '22.05.2017', 'Times': ['7-12', '13-17'], 'Comments': ['ein kommentar']}
        self.assertEqual([{'stubewdate': '22.05.2017', 'iststunden': '0700-1200', 'stubewbez': 'ein kommentar'}, {'stubewdate': '22.05.2017', 'iststunden': '1300-1700', 'stubewbez': 'ein kommentar'}], proles.create_data(day_entry))

        day_entry = {'Date': '22.05.2017', 'Times': ['7-12', '13-17'], 'Comments': ['ein kommentar', 'zweiter kommentar']}
        self.assertEqual([{'stubewdate': '22.05.2017', 'iststunden': '0700-1200', 'stubewbez': 'ein kommentar'}, {'stubewdate': '22.05.2017', 'iststunden': '1300-1700', 'stubewbez': 'zweiter kommentar'}], proles.create_data(day_entry))

if __name__ == "__main__":
    unittest.main()
