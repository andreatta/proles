@echo off
REM Batch script to start proles.py Python script on Windows

set USERNAME=Cyril Andreatta

for /f "usebackq tokens=*" %%i in (`"gpg -qd pw.gpg 2>nul"`) do (
	set PW=%%i
)

python proles.py -u "%USERNAME%" -p "%PW%" %*
