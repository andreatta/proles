# Proles script for adding daily working hours

## Setup

`Proles.py` is written in `Python 3` and should therefore run on a lot of operating systems (Linux, Unix, MacOS, Windows...).

It is depending on other software which has to be installed.

- Python (including some additional packages)
- GnuPG

### Needed Python Packages

The easiest way is probably to install all needed packages with the Python Package Manager
```
sudo pip install lxml beautifulsoup4 requests gnupg 
```

You could also install the packages through your distro's package manager.

#### For Debian/Ubuntu (this does also work for Bash Shell on Windows 10):
```
sudo apt-get install python-lxml python-bs4
```

#### For Arch:  
`You're smart enough, you'll figure it out.`
 
### GnuPG (GPG)
`GPG` is needed if you want to save your password encrypted, which is **HIGHLY** recommended.

#### Installation on Windows
For Windows the GPG Version 1.4 executable has to be installed from the following website.

[](https://gnupg.org/ftp/gcrypt/binary/gnupg-w32cli-1.4.21.exe)

After installation the `gpg.exe` has to be added to the `PATH` environment variable.

#### Create encrypted credentials file

Create a key, so you don't have to enter the passphrase everytime.  
The creation of the key is menu guided, so it's pretty straight forward.  
It's recommended to set a passphrase for each gpg-key, but if the generated key is only used for logging working times to Proles, there cannot be much harm done if no passphrase is set.
```
gpg --gen-key
```

Copy the number of your created key.  
It's that number on the first row after the key size.

```
gpg --list-keys
```
 
Encrypt a temporary file with your `Name` and `Lastname` seperated by one Space, on the first line and your `password` on the second line.  
These lines must correspond, with the login data for Proles.  
Save that file as `cred`.


Create an encrypted version of the file and delete the plain-text file.

```
gpg -e -r <GPG-KEY> cred
rm !$
```

TIP:
If you messed your key up, it can be edited.

```
gpg --edit-key <GPG-KEY>
save
```

## Usage

### Windows

On Windows, there was a problem with executing GPG directly from the `proles.py` script.
Therefore there exists a `proles.bat` which retrieves the encrypted password from `pw.gpg` (a file containing only the encrypted password) and the calls the Python script.

 ATTENTION! The Username has to be changed in `proles.bat`

### Add Times directly from Command Line

```
./proles.py -d <DATE> -t <FROM>-<TO> <FROM>-<TO> -c "What project you worked on"
```

If `<DATE>` is omitted current date is assumed.

`<FROM>` and `<TO>` are the times you started and ended your working session. You can add as many time-pairs as you'd like.  
Allowed formats are as follows (and mixtures of it):

```
7-12
0700-1200
07:00-12:00
```
Comment is mandatory and has to be within quotation marks. It is possible to add as many comments as there are time entries for the specific day.

### Add Times from file

Working hours can also be read from a text file.  
Each line has to start with a `<DATE>` followed by one or more `<FROM-TO>` blocks and end with at least one `"<COMMENT>"` in quotationmarks.
These items can be separated with `Tabs` or `Spaces`.

```
./proles.py -i timesheet.txt
```

## Proles Help Menu

```
usage: proles.py [-h] [-v] [-d DATE] [-t TIMES [TIMES ...]]
                 [-c COMMENTS [COMMENTS ...]] [-i IMPORT] [-g GPGFILE]
                 [-u USER] [-p PASSWORD] [-y]

Version 1.3. Add time entries to proles.ch web service.

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         Print version and exit
  -d DATE, --date DATE  Set date, if omitted current date is used
  -t TIMES [TIMES ...], --times TIMES [TIMES ...]
                        From-to working hours. It's possible to add multiple
                        time pairs for one day simultaniously
  -c COMMENTS [COMMENTS ...], --comments COMMENTS [COMMENTS ...]
                        Description of your work or the project you are
                        working on
  -i IMPORT, --import IMPORT
                        Import time entries from file. Each line should look
                        like this: DATE FROM-TO FROM-TO "COMMENT" "COMMENT"
  -g GPGFILE, --gpgfile GPGFILE
                        Load credentials from an encrypted file. If paramter
                        is omitted it tries to open `cred.gpg` in current
                        directory
  -u USER, --user USER  Username
  -p PASSWORD, --password PASSWORD
                        For improved security use "$(gpg -qd pw.gpg)", where
                        pw.gpg is a GnuPG encrypted file with only your
                        password
  -y, --dryrun          Do not actually send data to server
```
