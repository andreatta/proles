#!/usr/bin/env python
''' Add file for importing worked hours to proles.py '''

import os
import sys
from datetime import datetime

def create_filename(month, now):
    ''' Create filename from current month '''
    return month + '_' + now.strftime('%Y') + '.txt'


def new_file(filename, month, year):
    ''' Create new file if it is not existing '''
    with open(filename, 'a+') as file:
        file.write('# Proles.py Timesheet %s %s\n' % (month, year))


def ask_for_comment():
    ''' Ask the user for a description of the work done '''
    return input('=== Please describe the work you have done: ')

def main():
    ''' Add current time to file for later import through `proles.py` '''
    now = datetime.now()
    date = now.strftime('%d.%m.%Y')
    time = now.strftime('%H%M')
    month = now.strftime("%B")
    year = now.strftime('%Y')
    filename = create_filename(month, now)

    if not os.path.isfile(filename):
        print("Creating file %s" % filename)
        new_file(filename, month, year)

    # open file for read and write
    with open(filename, 'r+') as file:
        line = ''
        for line in file:
            pass

        # only check last line
        if len(line) > 1 and '#' not in line[0]:
            if line[0].isdigit():
                last = line.strip()

                # complete line with comment
                if last.endswith('"'):
                    new = '\n' + date + '\t' + time + '-'
                elif last.endswith('-'):
                    comment = ask_for_comment()
                    new = time
                    new += '\t"' + (comment) +'"'
                else:
                    print("Malformed line: " % last)
                    sys.exit(1)
        else:
            new = '\n' + date + '\t' + time + '-'

        file.write(new)

if __name__ == "__main__":
    main()
