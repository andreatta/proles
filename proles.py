#!/usr/bin/env python
''' Access timesheet on proles.ch server to add working hours. '''

import os
import sys
import datetime
import re
import json
import logging
import argparse
import requests
import gnupg

from bs4 import BeautifulSoup as bs

'''
TODO:
    - Error handling
    - Check arguments
    - Download Report (only fixed URL does work)
    - Add Vacation etc.
'''

'''
POST https://www.app2.proles.ch/net/includes/ajaxhandler/zeitErfassung.aspx HTTP/1.1
Accept: */*
Referer: https://www.app2.proles.ch/leistung/stundenerf.asp
Origin: https://www.app2.proles.ch
X-Requested-With: XMLHttpRequest
User-Agent: Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/538.1 (KHTML, like Gecko)
qutebrowser/0.10.1 Safari/538.1
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
'''

VERSION = '1.3'
LOG = logging
LOGLEVEL = logging.ERROR
# ATTENTION: Password is shown in plain-text if DEBUG is enabled
#LOGLEVEL = logging.DEBUG

# EXIT codes
_EXIT_SUCCESS = 0
_EXIT_MISSING_CRED = 1
_EXIT_MISSING_TIME = 2

# URLs to different sites
URL = 'https://www.app2.proles.ch/'
URL_ADDTIME = 'net/includes/ajaxhandler/zeitErfassung.aspx'
URL_REPORT = 'listen/listenAuswahlMa.asp?report=Stundendetails'
URL_REPORT_REDIR = "default.asp?sourceURL=/listen/listenAuswahlMa.asp&report=Stundendetails"
URL_DWL_REPORT = 'net/report/stundenDetails.aspx?guid={BEC1E4CA-7188-4589-824A-F85AE4B3748D}&read=true&kuNu=0&projektNu=0&datumVon=01.04.2017&datumBis=30.04.2017&report=Stundendetails&ausgabeBetrag=0&ausgabeFormat=0&berechtBetragSichtbar=0&zusammenfassungDrucken=False&uSchriDrucken=False&uSchriAuftrag=False&maGruNu=0&printKWTotal=False&SkipNonBillable=False&personMulti=&benuSprachCo=D'

def open_credential_file(filename):
    ''' Open file with saved credentials '''
    credkeys = ['userName', 'passwort']
    credentials = {
    }

    with open(filename) as credfile:
        for i, line in enumerate(credfile):
            credentials[credkeys[i]] = line.strip()

    return credentials


def login(credentials):
    ''' Login to proles.ch server with credentials read from file '''
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36'
    }

    # login
    session = requests.session()
    loginresponse = session.post(URL, data=credentials, headers=headers)

    #file = open('login.html', 'w')
    ##file.write(loginresponse.text.encode('utf-8'))
    #file.write(loginresponse.text)
    #file.close()

    #LOG.debug(loginresponse.status_code)

    if loginresponse.status_code == 200:
        if 'Passwort vergessen?' in loginresponse.text:
            raise ValueError('Login failed. Do you have the correct Username and password?')
        else:
            soup = bs(loginresponse.text, 'lxml')
            persnu = soup.find(id='ctl00_cph_hiddenPersNu')['value']
            manu = soup.find(id='ctl00_cph_hiddenMaNu')['value']
    else:
        print('Could not login!')

    return (session, {'persnu' : persnu, 'manu' : manu})


def create_report_filename():
    ''' Create nameto save report file '''
    filename = 'Proles_Time_'
    filename += '.pdf'

    return filename

def getreport(session):
    ''' Download report of working hours for current month '''
    reportheader = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36',
        'Referer' : 'https://www.app2.proles.ch/listen/listenAuswahlMa.asp?report=Stundendetails'
    }

    reportdata = {
        'cboMaGru' : '0',
        'cboPersonMulti' : '15902',
        'cboKunde' : '0',
        'cboProjekt' : '0',
        'datumVon' : '01.04.2017',
        'datumBis' : '30.04.2017',
        'cbodaterange' : '0',
        #'reportType' : 'crystal',
        'reportType' : 'excel',
        'hidMaAuswaehlen' : '',
        'allPersons' : '0,15902',
        'toggle' : '0'
    }

    '''
    reportresponse = session.get(URL+URL_DWL_REPORT, data=reportdata, headers=reportheader)
    print(reportresponse.status_code)
    print(reportresponse.headers)
    '''

    reportresponse = session.post(URL+URL_REPORT,
                                  data=reportdata,
                                  headers=reportheader,
                                  allow_redirects=False)
    print(reportresponse.history)
    print(reportresponse.url)
    print(reportresponse.headers)
    #print(reportresponse.text)

    #file = open(create_report_filename(), 'wb')
    ##file.write(reportresponse.content.encode('utf-8'))
    #file.write(reportresponse.content)
    #file.close()


def read_time_from_file(time_file, markadded):
    ''' Read working times from file '''
    if not time_file:
        raise ValueError('No file to read from')

    entry_list = []
    tmpfile = 'tmp'

    with open(time_file) as times:
        with open(tmpfile, 'w') as tmp:
            for line in times:
                if line[0] != '#' and line is not None and len(line) > 2:
                    entry = {'Date' : None, 'Times' : None, 'Comments' : None}
                    # get comments
                    startcom = line.index('"')
                    comments = line[startcom:].split('"')
                    comments = [x for x in comments if x != '' and x != ' ' and x != '\n']
                    entry['Comments'] = comments

                    # only date and time
                    entries = line[:startcom].split()
                    if len(entries) >= 2:
                        date = entries[0]
                        fromto = entries[1:]
                        entry['Date'] = date
                        entry['Times'] = fromto
                        entry_list.append(entry)
                        tmp.write('#' + line)
                else:
                    tmp.write(line)

    if markadded:
        os.rename(time_file, 'old_' + time_file)
        os.rename(tmpfile, time_file)
    else:
        os.remove(tmpfile)
    LOG.debug(entry_list)
    return entry_list


def create_data(day_entry):
    ''' Create data to be sent to proles server '''
    date = day_entry['Date']
    times = day_entry['Times']
    comments = day_entry['Comments']
    LOG.debug(day_entry)

    list_of_times = []

    for i, time in enumerate(times):
        data = {}
        data['stubewdate'] = sanitize_date(date)

        goodtimes = split_time(sanitize_time(time))
        data['iststunden'] = ''.join(goodtimes)

        # use last comment for following times if less comments than time entries
        if len(times) == len(comments):
            data['stubewbez'] = comments[i]
        else:
            data['stubewbez'] = comments[-1]

        list_of_times.append(data)

    LOG.debug(list_of_times)
    return list_of_times


def add_time(session, data):
    ''' Add time entry to proles server '''
    timeheader = {
        'referer' : 'https://www.app2.proles.ch/leistung/stundenerf.asp',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36'
    }

    # add time
    newtime = {
        'task' : 'add',
        'response' : 'json',
        'projektnu' : '128983',
        'teilprojektnu' : '142412',
        'aknu' : '469897',
        'akschrinu' : '496552',
        'buposnu' : '0',
    }

    newtime.update(data)

    timeresponse = session.post(URL+URL_ADDTIME, data=newtime, headers=timeheader)
    accepted = json.loads(timeresponse.text)

    if accepted and accepted[0]['Accepted'] is True:
        print('Time successfully added.')
    else:
        print('Time could not be added')
        print(accepted[0]['Message'])

    # debug
    #file = open('time.html', 'w')
    ##file.write(timeresponse.text.encode('utf-8'))
    #file.write(timeresponse.text)
    #file.close()


def split_time(timelist):
    ''' Split a list with multiple times into list with correctly formatted strings '''
    return ['-'.join([timelist[i], timelist[i+1]]) for i in range(0, len(timelist), 2)]


def sanitize_time(times):
    '''
    Check if this is a correct time string and returns a list with from-to times
    Return list of times
     '''
    #times = times.replace(' ', '-')
    times = times.replace(';', '-')
    times = times.replace(',', '-')
    times = times.strip().split('-')
    times = list(map(lambda t: re.sub(r'\D', '', t), times))

    # Try to smartly fill times with zeros and perform some checks on the input
    for i, time in enumerate(times):
        if len(time) == 1:
            times[i] = '0' + time + '00'

        if len(time) == 2:
            times[i] = time + '00'

        if len(time) == 3:
            times[i] = '0' + time

        if int(time) < 0 or int(time) > 2400:
            raise ValueError('Time entry %s is not valid!' % time)

        if i > 0 and int(times[i]) <= int(times[i - 1]):
            raise ValueError('End time %s must be later than start time %s' % (times[i], times[i - 1]))

    if len(times) % 2 != 0:
        raise ValueError('Odd number of time entries!')

    LOG.debug(times)
    return times


def sanitize_date(date):
    ''' Check format of date string '''
    date = re.sub(r'\D', '.', date)
    datelist = date.strip().split('.')

    if len(datelist) == 2:
        # add year if only 2 numbers (day, month) are given
        datelist.append(datetime.datetime.now().strftime('%Y'))

    if len(datelist) < 2 or len(datelist) > 3:
        raise ValueError('Wrong number of arguments for date')

    datelist[:2] = list(map(lambda d: d.zfill(2), datelist[:2]))
    LOG.debug(datelist)

    if len(datelist) == 3:
        if len(datelist[2]) == 2:
            datelist[2] = '20' + datelist[2]
        elif len(datelist[2]) != 4:
            raise ValueError('Wrong date format')

    if int(datelist[0]) < 1 or int(datelist[0]) > 31:
        raise ValueError('Value for day is out of range [1-31] %s' % datelist[0])

    if int(datelist[1]) < 1 or int(datelist[1]) > 12:
        raise ValueError('Value for month is out of range [1-12] %s' % datelist[1])

    if int(datelist[2]) < 2017 or int(datelist[2]) > 2040:
        raise ValueError('Value for year is out of range [2017-2040] %s' % datelist[2])

    return '.'.join(datelist)


def decrypt_file(gpgfile):
    ''' Decrypt file using GnuPG
    This does not work on Windows somehow '''
    gpg = gnupg.GPG()
    with open(gpgfile, 'rb') as file:
        plain = gpg.decrypt_file(file)

    return plain

def print_version():
    ''' print current version and exit '''
    print('%s Version %s' % (sys.argv[0], VERSION))
    sys.exit(_EXIT_SUCCESS)


def pretty_print(data):
    ''' Pretty print the data to add '''
    print(data['stubewdate'], data['iststunden'], data['stubewbez'])


def get_hours_minutes(time):
    ''' Get hours and minutes nicely formated '''
    sec = time.total_seconds()
    h = int(sec / 3600)
    m = int(sec / 60) % 60

    return "{:0>2}:{:0>2}".format(h, m)


def calculate_worked_hours(times):
    ''' Calculate time differences between coming and hoing times '''
    print('Worked:')
    total = datetime.timedelta()
    for t in times:
        timedifflist = t['iststunden'].split('-')
        fmt = '%H%M'
        tdelta = datetime.datetime.strptime(timedifflist[1], fmt) - datetime.datetime.strptime(timedifflist[0], fmt)

        total = total + tdelta
        print(get_hours_minutes(tdelta))

    print('Total worked hours %s' % get_hours_minutes(total))


def main():
    ''' main '''
    LOG.basicConfig(stream=sys.stderr, level=LOGLEVEL)
    today = datetime.datetime.now().strftime('%d.%m.%Y')

    parser = argparse.ArgumentParser(description=('Version %s. Add time entries to proles.ch web service.' % VERSION))
    #parser.add_mutually_exclusive_group()
    #parser.add_argument('-r', '--report', help='download time report', action='store_true')
    parser.add_argument('-v', '--version', help='Print version and exit', action='store_true')
    parser.add_argument('-d', '--date', help='Set date, if omitted current date is used', required=False, default=today)
    parser.add_argument('-t', '--times', help='From-to working hours. It\'s possible to add multiple time pairs for one day simultaniously', required=False, nargs='+')
    parser.add_argument('-c', '--comments', help='Description of your work or the project you are working on', required=False, nargs='+')
    parser.add_argument('-i', '--import', help='Import time entries from file. Each line should look like this: DATE FROM-TO FROM-TO "COMMENT" "COMMENT"', required=False)
    parser.add_argument('-g', '--gpgfile', help='Load credentials from an encrypted file. If paramter is omitted it tries to open `cred.gpg` in current directory', required=False, default='cred.gpg')
    parser.add_argument('-u', '--user', help='Username', required=False)
    parser.add_argument('-p', '--password', help='For improved security use "$(gpg -qd pw.gpg)", where pw.gpg is a GnuPG encrypted file with only your password', required=False)
    parser.add_argument('-y', '--dryrun', help='Do not actually send data to server', action='store_true')

    args = vars(parser.parse_args())

    if args['version'] is True:
        print_version()

    # Check if credentials have been supplied
    cred = {}

    if args['user'] is not None and args['password'] is not None:
        cred['userName'] = args['user']
        cred['passwort'] = args['password']
    elif args['gpgfile'] is not None and os.path.isfile(args['gpgfile']):
        plain = decrypt_file(args['gpgfile'])
        credlist = str(plain).strip().split('\n')
        # last element is a newline
        if len(credlist) >= 2:
            cred['userName'] = credlist[0]
            cred['passwort'] = credlist[1]
        else:
            raise ValueError('Credentials file has wrong format.')
    else:
        print('Supply credentials, either through a gpg encrypted file "-g" or with username "-u" and password "-p" as parameter.')
        sys.exit(_EXIT_MISSING_CRED)

    # dict that takes date, times and comments for a whole day
    day_entry = {}
    multiple_day_entries = []

    # take input either form file or from arguments
    if args['import'] is not None:
        multiple_day_entries = read_time_from_file(args['import'], not args['dryrun'])
    elif args['times'] is not None:
        # get worked hours, and add all available time pairs
        # todo -> error handling on missing arguments
        day_entry['Date'] = args['date']
        day_entry['Times'] = args['times']
        day_entry['Comments'] = args['comments']
        multiple_day_entries.append(day_entry)
    else:
        print('Add working hours either through command line arguments `-t` or with importing a file `-i`.')
        sys.exit(_EXIT_MISSING_TIME)

    # Try to login to proles.ch with credentials from file,
    # then add it to data dict needed to send data to proles
    session, ldict = login(cred)
    data = {}
    data.update(ldict)

    if args['comments'] is not None or args['import'] is not None:
        for day_entry in multiple_day_entries:
            times_list = create_data(day_entry)
            print('--- adding time ---')

            for time_entry in times_list:
                data.update(time_entry)

                pretty_print(time_entry)

                if args['dryrun']:
                    print('=== DRY RUN ===')
                else:
                    # send time entries for a day to proles
                    add_time(session, data)
            calculate_worked_hours(times_list)
    else:
        print('A short description about the work you have done is mandatory.')

    #if args['report'] is True:
    #  print('Get Report')
    #  getreport(session)

if __name__ == '__main__':
    main()
    sys.exit(_EXIT_SUCCESS)
